package modul2;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Orar {

	@Id
    private Integer id_Obiect;
    private int id_Grupa;
    private Date Data;
    private Time[] interval_Orar;
    private String sala_Curs;

    @OneToMany
    private Collection<Obiect> orarObiecte=new ArrayList<Obiect>();
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Data == null) ? 0 : Data.hashCode());
		result = prime * result + id_Grupa;
		result = prime * result
				+ ((id_Obiect == null) ? 0 : id_Obiect.hashCode());
		result = prime * result + Arrays.hashCode(interval_Orar);
		result = prime * result
				+ ((orarGrupe == null) ? 0 : orarGrupe.hashCode());
		result = prime * result
				+ ((orarObiecte == null) ? 0 : orarObiecte.hashCode());
		result = prime * result
				+ ((sala_Curs == null) ? 0 : sala_Curs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orar other = (Orar) obj;
		if (Data == null) {
			if (other.Data != null)
				return false;
		} else if (!Data.equals(other.Data))
			return false;
		if (id_Grupa != other.id_Grupa)
			return false;
		if (id_Obiect == null) {
			if (other.id_Obiect != null)
				return false;
		} else if (!id_Obiect.equals(other.id_Obiect))
			return false;
		if (!Arrays.equals(interval_Orar, other.interval_Orar))
			return false;
		if (orarGrupe == null) {
			if (other.orarGrupe != null)
				return false;
		} else if (!orarGrupe.equals(other.orarGrupe))
			return false;
		if (orarObiecte == null) {
			if (other.orarObiecte != null)
				return false;
		} else if (!orarObiecte.equals(other.orarObiecte))
			return false;
		if (sala_Curs == null) {
			if (other.sala_Curs != null)
				return false;
		} else if (!sala_Curs.equals(other.sala_Curs))
			return false;
		return true;
	}

	@OneToMany
    private Collection<Grupa> orarGrupe=new ArrayList<Grupa>();
    
    public int getId_Obiect() {
        return id_Obiect;
    }

    public Collection<Obiect> getOrarObiecte() {
		return orarObiecte;
	}

	public void setOrarObiecte(Collection<Obiect> orarObiecte) {
		this.orarObiecte = orarObiecte;
	}

	public Collection<Grupa> getOrarGrupe() {
		return orarGrupe;
	}

	public void setOrarGrupe(Collection<Grupa> orarGrupe) {
		this.orarGrupe = orarGrupe;
	}

	public void setId_Obiect(Integer id_Obiect) {
		this.id_Obiect = id_Obiect;
	}

	public void setId_Obiect(int id_Obiect) {
        this.id_Obiect = id_Obiect;
    }

    public int getId_Grupa() {
        return id_Grupa;
    }

    public void setId_Grupa(int id_Grupa) {
        this.id_Grupa = id_Grupa;
    }

    public Date getData() {
        return Data;
    }

    public void setData(Date data) {
        Data = data;
    }

    public Time[] getInterval_Orar() {
        return interval_Orar;
    }

    public void setInterval_Orar(Time[] interval_Orar) {
        this.interval_Orar = interval_Orar;
    }

    public String getSala_Curs() {
        return sala_Curs;
    }

    public void setSala_Curs(String sala_Curs) {
        this.sala_Curs = sala_Curs;
    }

}
