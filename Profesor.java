package modul2;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQuery(
	    name="findProfesorBy_Nume",
	    query="SELECT Nume  FROM Profesor  WHERE Nume=:Nume")
public class Profesor {

	@Id
	private Integer id_Profesor;
	@OneToMany
	private Collection<Obiect> obiecteProfesor=new ArrayList<Obiect>();
	
    
	
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((id_Profesor == null) ? 0 : id_Profesor.hashCode());
		result = prime * result
				+ ((obiecteProfesor == null) ? 0 : obiecteProfesor.hashCode());
		result = prime * result
				+ ((site_Persoanal == null) ? 0 : site_Persoanal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profesor other = (Profesor) obj;
		if (id_Profesor == null) {
			if (other.id_Profesor != null)
				return false;
		} else if (!id_Profesor.equals(other.id_Profesor))
			return false;
		if (obiecteProfesor == null) {
			if (other.obiecteProfesor != null)
				return false;
		} else if (!obiecteProfesor.equals(other.obiecteProfesor))
			return false;
		if (site_Persoanal == null) {
			if (other.site_Persoanal != null)
				return false;
		} else if (!site_Persoanal.equals(other.site_Persoanal))
			return false;
		return true;
	}

	public Collection<Obiect> getObiecteProfesor() {
		return obiecteProfesor;
	}

	public void setObiecteProfesor(Collection<Obiect> obiecteProfesor) {
		this.obiecteProfesor = obiecteProfesor;
	}

	public void setId_Profesor(Integer id_Profesor) {
		this.id_Profesor = id_Profesor;
	}

	private String Nume;
    private String Prenume;
    private String Email;

    public int getId_Profesor() {
        return id_Profesor;
    }


    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public String getPrenume() {
        return Prenume;
    }

    public void setPrenume(String prenume) {
        Prenume = prenume;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public URL getSite_Persoanal() {
        return site_Persoanal;
    }

    public void setSite_Persoanal(URL site_Persoanal) {
        this.site_Persoanal = site_Persoanal;
    }

    private URL site_Persoanal;
}
