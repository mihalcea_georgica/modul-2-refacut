package modul2;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;

@Entity
@PersistenceContext
public class Catalog {

	@Id
	private Integer id_Catalog;
    private int id_Student;
    private int id_Obiect;
    private int Nota;
    private Date data_Notei;
    private int nr_Prezente;

    public int getId_Student() {
        return id_Student;
    }

    @OneToMany
    private Collection<Student> studentByCatalog=new ArrayList<Student>();
 
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Nota;
		result = prime * result
				+ ((data_Notei == null) ? 0 : data_Notei.hashCode());
		result = prime * result
				+ ((id_Catalog == null) ? 0 : id_Catalog.hashCode());
		result = prime * result + id_Obiect;
		result = prime * result + id_Student;
		result = prime * result + nr_Prezente;
		result = prime
				* result
				+ ((obiecteInCatalog == null) ? 0 : obiecteInCatalog.hashCode());
		result = prime
				* result
				+ ((studentByCatalog == null) ? 0 : studentByCatalog.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Catalog other = (Catalog) obj;
		if (Nota != other.Nota)
			return false;
		if (data_Notei == null) {
			if (other.data_Notei != null)
				return false;
		} else if (!data_Notei.equals(other.data_Notei))
			return false;
		if (id_Catalog == null) {
			if (other.id_Catalog != null)
				return false;
		} else if (!id_Catalog.equals(other.id_Catalog))
			return false;
		if (id_Obiect != other.id_Obiect)
			return false;
		if (id_Student != other.id_Student)
			return false;
		if (nr_Prezente != other.nr_Prezente)
			return false;
		if (obiecteInCatalog == null) {
			if (other.obiecteInCatalog != null)
				return false;
		} else if (!obiecteInCatalog.equals(other.obiecteInCatalog))
			return false;
		if (studentByCatalog == null) {
			if (other.studentByCatalog != null)
				return false;
		} else if (!studentByCatalog.equals(other.studentByCatalog))
			return false;
		return true;
	}

	@OneToMany
    private Collection<Obiect> obiecteInCatalog=new ArrayList<Obiect>();
 
    
    public Integer getId_Catalog() {
		return id_Catalog;
	}

	public void setId_Catalog(Integer id_Catalog) {
		this.id_Catalog = id_Catalog;
	}

	public Collection<Student> getStudentByCatalog() {
		return studentByCatalog;
	}

	public void setStudentByCatalog(Collection<Student> studentByCatalog) {
		this.studentByCatalog = studentByCatalog;
	}

	public Collection<Obiect> getObiecteInCatalog() {
		return obiecteInCatalog;
	}

	public void setObiecteInCatalog(Collection<Obiect> obiecteInCatalog) {
		this.obiecteInCatalog = obiecteInCatalog;
	}

	public void setId_Student(int id_Student) {
        this.id_Student = id_Student;
    }

    public int getId_Obiect() {
        return id_Obiect;
    }

    public void setId_Obiect(int id_Obiect) {
        this.id_Obiect = id_Obiect;
    }

    public int getNota() {
        return Nota;
    }

    public void setNota(int nota) {
        Nota = nota;
    }

    public Date getData_Notei() {
        return data_Notei;
    }

    public void setData_Notei(Date data_Notei) {
        this.data_Notei = data_Notei;
    }

    public int getNr_Prezente() {
        return nr_Prezente;
    }

    public void setNr_Prezente(int nr_Prezente) {
        this.nr_Prezente = nr_Prezente;
    }
}
