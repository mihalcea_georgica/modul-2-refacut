package modul2;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Obiect {

	@Id
    private Integer id_Obiect;
	
    private int id_Profesor;
	
    
    @OneToMany(cascade=CascadeType.ALL)
	private Collection<Evaluare> evaluari=new ArrayList<Evaluare>();
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Credite;
		result = prime * result + (Prezenta ? 1231 : 1237);
		result = prime * result
				+ ((evaluari == null) ? 0 : evaluari.hashCode());
		result = prime * result + id_Evaluare;
		result = prime * result
				+ ((id_Obiect == null) ? 0 : id_Obiect.hashCode());
		result = prime * result + id_Profesor;
		result = prime * result
				+ ((profesorObiect == null) ? 0 : profesorObiect.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Obiect other = (Obiect) obj;
		if (Credite != other.Credite)
			return false;
		if (Prezenta != other.Prezenta)
			return false;
		if (evaluari == null) {
			if (other.evaluari != null)
				return false;
		} else if (!evaluari.equals(other.evaluari))
			return false;
		if (id_Evaluare != other.id_Evaluare)
			return false;
		if (id_Obiect == null) {
			if (other.id_Obiect != null)
				return false;
		} else if (!id_Obiect.equals(other.id_Obiect))
			return false;
		if (id_Profesor != other.id_Profesor)
			return false;
		if (profesorObiect == null) {
			if (other.profesorObiect != null)
				return false;
		} else if (!profesorObiect.equals(other.profesorObiect))
			return false;
		return true;
	}

	public Collection<Evaluare> getEvaluari() {
		return evaluari;
	}

	public void setEvaluari(Collection<Evaluare> evaluari) {
		this.evaluari = evaluari;
	}

	public Profesor getProfesorObiect() {
		return profesorObiect;
	}

	public void setProfesorObiect(Profesor profesorObiect) {
		this.profesorObiect = profesorObiect;
	}

	public void setId_Obiect(Integer id_Obiect) {
		this.id_Obiect = id_Obiect;
	}

	private int id_Evaluare;
	
    private String Nume;
    private int Credite;
    private boolean Prezenta;
    
    @ManyToOne
    private Profesor profesorObiect=new Profesor();

    public int getId_Obiect() {
        return id_Obiect;
    }

    public void setId_Obiect(int id_Obiect) {
        this.id_Obiect = id_Obiect;
    }

    public int getId_Profesor() {
        return id_Profesor;
    }

    public void setId_Profesor(int id_Profesor) {
        this.id_Profesor = id_Profesor;
    }

    public int getId_Evaluare() {
        return id_Evaluare;
    }

    public void setId_Evaluare(int id_Evaluare) {
        this.id_Evaluare = id_Evaluare;
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public int getCredite() {
        return Credite;
    }

    public void setCredite(int credite) {
        Credite = credite;
    }

    public boolean isPrezenta() {
        return Prezenta;
    }

    public void setPrezenta(boolean prezenta) {
        Prezenta = prezenta;
    }
}
