package modul2;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@NamedQuery(
	    name="SelectAllStudentFromJoin_Grupa+Student",
	    query="SELECT nume,prenume,nr_Matricol,id_Student,id_Grupa,an_Studiu FULL OUTER JOIN Grupa ON Student.id_Grupa=Grupa.id_Grupa")
@Entity
public class Grupa {

	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer Id_Grupa;
	
	@OneToMany
	private Collection<Student>StudentiByGrupa=new ArrayList<Student>();
 

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((Id_Grupa == null) ? 0 : Id_Grupa.hashCode());
		result = prime * result
				+ ((StudentiByGrupa == null) ? 0 : StudentiByGrupa.hashCode());
		result = prime * result
				+ ((nume_Grupa == null) ? 0 : nume_Grupa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupa other = (Grupa) obj;
		if (Id_Grupa == null) {
			if (other.Id_Grupa != null)
				return false;
		} else if (!Id_Grupa.equals(other.Id_Grupa))
			return false;
		if (StudentiByGrupa == null) {
			if (other.StudentiByGrupa != null)
				return false;
		} else if (!StudentiByGrupa.equals(other.StudentiByGrupa))
			return false;
		if (nume_Grupa == null) {
			if (other.nume_Grupa != null)
				return false;
		} else if (!nume_Grupa.equals(other.nume_Grupa))
			return false;
		return true;
	}

	public Collection<Student> getStudentiByGrupa() {
		return StudentiByGrupa;
	}

	public void setStudentiByGrupa(Collection<Student> studentiByGrupa) {
		StudentiByGrupa = studentiByGrupa;
	}

	public void setId_Grupa(Integer id_Grupa) {
		Id_Grupa = id_Grupa;
	}

	private String nume_Grupa;

    public Grupa(String nume)
    {
    	this.nume_Grupa=nume;
    }
    
    public Grupa()
    {}
    
    public int getId_Grupa() {
        return Id_Grupa;
    }

    public void setId_Grupa(int id_Grupa) {
         this.Id_Grupa = id_Grupa;
    }

    public String getNume_Grupa() {
        return nume_Grupa;
    }

    public void setNume_Grupa(String nume_Grupa) {
        this.nume_Grupa = nume_Grupa;
    }
}
