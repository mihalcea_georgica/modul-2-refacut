package modul2;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Evaluare {

	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
    private Integer id_Evaluare;

	@OneToMany(cascade=CascadeType.ALL)
	private Collection<Obiect> Obiecte=new ArrayList<Obiect>();
	
	
    public Collection<Obiect> getObiecte() {
		return Obiecte;
	}

	public void setObiecte(Collection<Obiect> obiecte) {
		Obiecte = obiecte;
	}

	private String tip_Evaluare;

    public Integer getId_Evaluare() {
        return id_Evaluare;
    }

    public void setId_Evaluare(Integer id_Evaluare) {
        this.id_Evaluare = id_Evaluare;
    }

    public String getTip_Evaluare() {
        return tip_Evaluare;
    }

    public void setTip_Evaluare(String tip_Evaluare) {
        this.tip_Evaluare = tip_Evaluare;
    }
}
