package modul2;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity

@NamedQuery(
	    name="findStudentBy_NrMatricol",
	    query="SELECT Nume  FROM Student  WHERE nr_Matricol=:nr_Matricol")

public class Student {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id_Student;
	
    private int id_Grupa;
	
	@ManyToOne
	private Grupa grupaStudent=new Grupa();
	
    public Grupa getGrupaStudent() {
		return grupaStudent;
	}
	public void setGrupaStudent(Grupa grupaStudent) {
		this.grupaStudent = grupaStudent;
	}
	public void setId_Student(Integer id_Student) {
		this.id_Student = id_Student;
	}

	private String Nume;
    private String Prenume;
    private int an_Studiu;
    private int nr_Matricol;
    private String email;


    public Student()
    {
    }
    public Student(String nume,String Prenume,int anStudiu,int nrMatricol,String email)
    {
    	this.Nume=nume;this.Prenume=Prenume;
    	this.an_Studiu=anStudiu;this.nr_Matricol=nrMatricol;
    	this.email=email;
    }
    public int getId_Student() {
        return id_Student;
    }

    public void setId_Student(int id_Student) {
        this.id_Student = id_Student;
    }

    public int getId_Grupa() {
        return id_Grupa;
    }

    public void setId_Grupa(int id_Grupa) {
        this.id_Grupa = id_Grupa;
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public String getPrenume() {
        return Prenume;
    }

    public void setPrenume(String prenume) {
        Prenume = prenume;
    }

    public int getAn_Studiu() {
        return an_Studiu;
    }

    public void setAn_Studiu(int an_Studiu) {
        this.an_Studiu = an_Studiu;
    }

    public int getNr_Matricol() {
        return nr_Matricol;
    }

    public void setNr_Matricol(int nr_Matricol) {
        this.nr_Matricol = nr_Matricol;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((grupaStudent == null) ? 0 : grupaStudent.hashCode());
		result = prime * result + id_Grupa;
		result = prime * result
				+ ((id_Student == null) ? 0 : id_Student.hashCode());
		result = prime * result + nr_Matricol;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (grupaStudent == null) {
			if (other.grupaStudent != null)
				return false;
		} else if (!grupaStudent.equals(other.grupaStudent))
			return false;
		if (id_Grupa != other.id_Grupa)
			return false;
		if (id_Student == null) {
			if (other.id_Student != null)
				return false;
		} else if (!id_Student.equals(other.id_Student))
			return false;
		if (nr_Matricol != other.nr_Matricol)
			return false;
		return true;
	}

}
