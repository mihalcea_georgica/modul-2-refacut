package modul2;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class News {

	@Id
    private int id_News;
    private String Titlu;
    private String Continut;
    private Date data_Aparitiei;

    public int getId_News() {
        return id_News;
    }

    public void setId_News(int id_News) {
        this.id_News = id_News;
    }

    public String getTitlu() {
        return Titlu;
    }

    public void setTitlu(String titlu) {
        Titlu = titlu;
    }

    public String getContinut() {
        return Continut;
    }

    public void setContinut(String continut) {
        Continut = continut;
    }

    public Date getData_Aparitiei() {
        return data_Aparitiei;
    }

    public void setData_Aparitiei(Date data_Aparitiei) {
        this.data_Aparitiei = data_Aparitiei;
    }
}
