package teste.JUnit;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import modul2.Grupa;
import modul2.Profesor;
import modul2.Student;

import org.junit.Test;

import junit.framework.TestCase;

public class Teste{

	
	private javax.persistence.EntityManagerFactory emf;
    private static javax.persistence.EntityManager em;
    private static final String PERSISTENCE_UNIT_NAME = "bazadatehibernate";
    
	public <T> void comitEntity(T entityToCommit)
	{
	   em.persist(entityToCommit);
	}
	public void initEntityManager()
	{
		Map<String, String> properties = new HashMap<String, String>();
        properties.put("javax.persistence.jdbc.user", "root");
        properties.put("javax.persistence.jdbc.password", "admin");
        emf = javax.persistence.Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME,properties);
        em = emf.createEntityManager();
	}

	@Test
	public void namedQueryAddStudent()
	{
		initEntityManager();
	
		Student studentPersistat=new Student("Popescu","Ionel",3,2345,"popescu.ionel@gmail.com");
		
		em.getTransaction().begin();
		
		comitEntity(studentPersistat);
		
		em.flush();
		
		Query interogare=em.createNamedQuery("findStudentBy_NrMatricol").setParameter("nr_Matricol", 2345);
		
		String rezultat=(String) interogare.getSingleResult();
		
		assertEquals("Popescu",rezultat);
		
		 em.close();
	     emf.close();
				
	}
	@Test
	public void namedQuerySelectProfesor() throws MalformedURLException
	{
		initEntityManager();
		em.getTransaction().begin();
		

		Profesor profesorPersistat=new Profesor();
		profesorPersistat.setEmail("mirceaA@tuiasi.ro");
		profesorPersistat.setNume("Mircea");
		profesorPersistat.setPrenume("Alexandru");
		profesorPersistat.setSite_Persoanal(new URL("http://mirceaAlexandru.tuiasi.ro"));
		profesorPersistat.setId_Profesor(1);
		
		comitEntity(profesorPersistat);
		
		em.flush();
		
		Query interogare=em.createNamedQuery("findProfesorBy_Nume").setParameter("Nume","Mircea");
		String rezultat=(String) interogare.getSingleResult();
		
		assertEquals("Mircea",rezultat);
		
		 em.close();
	     emf.close();
	}
	@Test
	public void namedQueryStudentiGrupa()
	{
		initEntityManager();
		em.getTransaction().begin();
		
		Grupa grupaPersistata=new Grupa("1303B");
		
		Student s1=new Student("Mihalache","Vlad",4,2346,"mihalache.Vlad@gmail.com");
		Student s2=new Student("Andronic","Mihai",4,2347,"and.Claudiu@gmail.com");
		Collection<Student> studentiPersistati=new ArrayList<Student>();
		
		studentiPersistati.add(s1);
		studentiPersistati.add(s2);
		grupaPersistata.setStudentiByGrupa(studentiPersistati);
		
		
		Query interogare=em.createNamedQuery("SelectAllStudentFromJoin_Grupa+Student").setParameter("grupa", grupaPersistata);
		
		@SuppressWarnings("unchecked")
		Collection<Student> listaReturnataQuery=(Collection<Student>) interogare.getSingleResult();
		
		assertEquals(studentiPersistati,listaReturnataQuery);
		 em.close();
	     emf.close();
	}
	@Test
	public void nativeRetrieveStudent()
	{
		initEntityManager();
		
		Student studentPersistat=new Student("Mihnea","Andrei",2,2325,"mihnea.andrei@gmail.com");
		
		em.getTransaction().begin();
		
		comitEntity(studentPersistat);
		
		em.flush();
		
		String interogare="INSERT INTO Student (nume,prenume ,an_Studiu,nr_Matricol,email) VALUES(?,?,?,?,?)";
				
		Query interogareQuerry=em.createNativeQuery(interogare,Student.class);
		interogareQuerry.setParameter(1, "Mihnea");
		interogareQuerry.setParameter(2,"Andrei");
		interogareQuerry.setParameter(3,2);
		interogareQuerry.setParameter(4,2325);
		interogareQuerry.setParameter(5,"mihnea.andrei@gmail.com");
		
		interogareQuerry.executeUpdate();
		
		Query interogareQuerry2=
				em.createQuery("Select Nume from Student where nr_Matricol=2325");
		
		String rezultat=(String) interogareQuerry.getSingleResult();
		
		//System.out.println(rezultat);
		
		 assertEquals("Mihnea",rezultat);
		 em.close();
	     emf.close();
	}
	@Test
	public void nativeRetriveProfesor() throws MalformedURLException
	{
		initEntityManager();
		em.getTransaction().begin();
		
		
		
		String interogare="INSERT INTO PROFESOR (Email,Nume,Prenume,site_Persoanal,id_Profesor) VALUES(?,?,?,?,?)";
		
		Query interogareQuery=em.createNativeQuery(interogare, Profesor.class);
		interogareQuery.setParameter(1, "mirceaA@tuiasi.ro");
		interogareQuery.setParameter(2, "Mircea");
		interogareQuery.setParameter(3, "Alexandru");
		interogareQuery.setParameter(4, new URL("http://mirceaAlexandru.tuiasi.ro"));
		interogareQuery.setParameter(5, 12);
		
		interogareQuery.executeUpdate();
	

		Query rezultat=em.createQuery("SELECT Nume FROM Profesor WHERE  Prenume='Alexandru'");
		String numeProf=(String) rezultat.getSingleResult();
		
		assertEquals("Mircea",numeProf);
	}
}

